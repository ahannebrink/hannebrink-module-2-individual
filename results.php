<!DOCTYPE html>
<html>
<body>
	<?php 
		$num1 = $_GET["num1"];
		$num2 = $_GET["num2"];
		$operation = $_GET["operation"];
	?>

	Answer: <br>
	<?php
			if ($operation == 'add') {
				$sum = $num1 + $num2;
				echo "$num1 + $num2 = $sum";
			}
			if ($operation == 'subtract') {
				$dif = $num1 - $num2;
				echo "$num1 - $num2 = $dif";
			}
			if ($operation == 'multiply') {
				$product = $num1 * $num2;
				echo "$num1 * $num2 = $product";
			}
			if ($operation == 'divide') {
				if ($num2 == 0) {
					echo "Don't divide by zero!";
				}
				else {
					$quotient = $num1 / $num2;
					echo "$num1 / $num2 = $quotient";
				}
			}
	?> 

</body>
</html
